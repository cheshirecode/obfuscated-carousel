import template from './template.js';
/**
 * [description]
 * @return {[type]} [description]
 */
export default function ({
  name,
  description,
  src,
  licenseLabel,
  licenseNumber,
  agentCount,
  totalProperties,
  phone,
  links: {
    self,
    logo,
    logo2x,
  },
  location,
  residentialForRentCount,
  residentialForSaleCount,
  commercialForRentCount,
  commercialForSaleCount,
  commercialTotalCount,
} = {}) {
  const rentalDetails = template `
    Residential for rent ${residentialForRentCount}
    Residential for sale ${residentialForSaleCount}
    Commercial for rent ${commercialForRentCount}
    Commercial for sale ${commercialForSaleCount}
  `;
  return template `
    <article class="slide">
      <div class="name">
        <h3>
          ${name}
        </h3>
      </div>
      <hr />
      <div class="infoCard">
        <a href="https://www.propertyfinder.ae${self}" target="_blank">
          <div class="img" style="background:url('${logo}') center center"/>
          </div>
        </a>
        <div class="line">
          <div class="field">
            <span class="paramName">Location</span>
            <span class="value">${location}</span>
          </div>
          <div class="field">
            <span class="paramName">${licenseLabel}</span>
            <span class="value">${licenseNumber}</span>
          </div>
          <div class="field">
            <span class="paramName">
              <i class="fa fa-home" title="${rentalDetails}"></i>
            </span>
            <span class="value">${totalProperties}</span>
          </div>
          <div class="field">
            <span class="paramName">
              <i class="fa fa-user-secret" title="Agent(s)"></i>
            </span>
            <span class="value">${agentCount}</span>
          </div>
          <div class="field">
            <span class="paramName">
              <i class="fa fa-phone"></i>
            </span>
            <span class="value">${phone}</span>
          </div>
        </div>
        <div class="description">
          ${description}
        </div>
      </div>
    </article>`;
};
