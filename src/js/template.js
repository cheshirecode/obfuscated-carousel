
// HTML Escape helper utility - https://developers.google.com/web/updates/2015/01/ES6-Template-Strings
const util = (() => {
  // Thanks to Andrea Giammarchi
  const reEscape = /[&<>'"]/g;
  const reUnescape = /&(?:amp|#38|lt|#60|gt|#62|apos|#39|quot|#34);/g;
  const oEscape = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '\'': '&#39;',
    '"': '&quot;',
  };
  const oUnescape = {
    '&amp;': '&',
    '&#38;': '&',
    '&lt;': '<',
    '&#60;': '<',
    '&gt;': '>',
    '&#62;': '>',
    '&apos;': '\'',
    '&#39;': '\'',
    '&quot;': '"',
    '&#34;': '"',
  };
  const fnEscape = function (m) {
    return oEscape[m];
  };
  const fnUnescape = function (m) {
    return oUnescape[m];
  };
  const replace = String.prototype.replace;
  return (Object.freeze || Object)({
    escape: function escape(s) {
      return replace.call(`${s}`, reEscape, fnEscape);
    },
    unescape: function unescape(s) {
      return replace.call(`${s}`, reUnescape, fnUnescape);
    },
  });
})();
/**
 * [description]
 * @return {[type]} [description]
 */
export default function (...args) {
  // Tagged template function
  const pieces = args[0];
  let result = pieces[0];
  const substitutions = args.slice(1);
  for (let i = 0; i < substitutions.length; ++i) {
    result += util.escape(substitutions[i]) + pieces[i + 1];
  }

  return result;
}
