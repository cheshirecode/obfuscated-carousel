if (module.hot) {
  module.hot.accept();
}

import 'babel-polyfill';
require('font-awesome-webpack');
import '../styles/index.scss';

import get from './get';
import slideWithProfileView from './slideWithProfileView';
import { namedId, carousel, nextLinkedItem, previousLinkedItem } from './carousel';
import { create as createStyleSheet, addCSSRule } from './styleSheet';
import nextArrow from '../assets/next.png';
import prevArrow from '../assets/prev.png';
let existingCSSStyleSheetName = '';
let carouselEl = document.querySelectorAll('#carousel')[0]; // assuming it exists
const API_PATH = process.env.NODE_ENV.toLowerCase() === 'development'
  ? 'ajax/search?page=1'
  : 'https://www.propertyfinder.ae/en/find-broker/ajax/search?page=1';

window.init = () => get(API_PATH)
  .then(function (data) {
    const { CSSStyleSheet, name } = createStyleSheet();
    // if there is an existing CSS stylesheet from previous load, remove it to make place to new one
    if (existingCSSStyleSheetName) {
      const node = document.querySelectorAll(`style[name="${existingCSSStyleSheetName}"]`)[0];
      node && node.parentNode.removeChild(node);
    }
    existingCSSStyleSheetName = name;
    const newData = data.data.slice(0);
    const itemCount = newData.length;
    const slides = newData.map((d, i) => {
      let selector = `#${namedId(i)}:checked ~ #slides .inner`;
      let rule = `margin-left:-${i * 100}%;`;
      addCSSRule(CSSStyleSheet, selector, rule);
      // these custom CSS need to be generated based on list of items returned from API
      // so we compute them here and add to the dynamic stylesheet
      selector = `#${namedId(i)}:checked ~
        #controls label:nth-child(${nextLinkedItem(i, itemCount)})`;
      rule = `
        background: url(${nextArrow}) no-repeat;
        float: right;
        margin: 0 -70px 0 0;
        display: block;
      `;
      addCSSRule(CSSStyleSheet, selector, rule);

      selector = `#${namedId(i)}:checked ~
        #controls label:nth-child(${previousLinkedItem(i, itemCount)})`;
      rule = `
        background: url(${prevArrow}) no-repeat;
        float: left;
        margin: 0 0 0 -70px;
        display: block;
      `;
      addCSSRule(CSSStyleSheet, selector, rule);

      return slideWithProfileView(d);
    });
    // same for the width of the container, and each slide
    addCSSRule(CSSStyleSheet, `#slides .inner`, `width: ${100 * itemCount}%`);
    addCSSRule(CSSStyleSheet, `#slides article`, `width: ${100 / itemCount}%`);
    const carouselHTML = carousel({
      slides,
    });
    // finally replace the DOM with the carousel
    carouselEl.innerHTML = carouselHTML;
  });

window.init();
