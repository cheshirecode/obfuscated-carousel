/**
 * Return a random enough UUID
 *
 * @param  {String} salt [description]
 * @return {[type]}      [description]
 */
export default function () {
  return Math.random().toString(36).slice(2, 23) + Math.random().toString(32).slice(2, 14);
};
