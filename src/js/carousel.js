import template from './template.js';
export const namedId = (i) => `slide${i + 1}`;
export const nextLinkedItem = (i, l) => (i + 1) % l + 1;
export const previousLinkedItem = (i, l) => (i ? i : l);
/**
 * [description]
 * @return {[type]} [description]
 */
export const carousel = ({
  checked = 0, // zero-index of the current item
  slides,
} = {}) => {
  const inputs = slides.map((d, i) => template `
    <input ${checked === i ? 'checked' : ''} type="radio" name="slider" id="${namedId(i)}"
      class="slideSelection" 
    />
  `).join('');
  const labels = slides.map((d, i) => template `
    <label for="${namedId(i)}"></label>
  `).join('');
  return template `
  <article id="slider">`
    + inputs
    + template `
    <!-- The Slider -->
    <div id="slides">  
      <div id="overflow">
        <div class="inner">`
          + slides.join('')
        + template`     
        </div> <!-- .inner -->
      </div> <!-- #overflow -->
    </div> <!-- #slides -->
    <!-- Controls and Active Slide Display -->
    <div id="controls">`
      + labels
    + template `
    </div> <!-- #controls -->
    <div id="active">`
      + labels
    + template `
    </div> <!-- #active -->
  </article> <!-- #slider -->
  `;
};
