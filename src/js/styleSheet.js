import randomUUID from './randomUUID.js';

// modified method from https://davidwalsh.name/add-rules-stylesheets
/**
 * [description]
 * @param  {[type]} name [description]
 * @return {[type]}      [description]
 */
const create = (name = randomUUID()) => {
  // Create the <style> tag
  const style = document.createElement('style');
  // Add a media (and/or media query) here if you'd like!
  // style.setAttribute("media", "screen")
  // style.setAttribute("media", "only screen and (max-width : 1024px)")

  // WebKit hack :(
  style.appendChild(document.createTextNode(''));

  // Add the <style> element to the page
  document.head.appendChild(style);
  style.setAttribute('name', name); // use it to search for added stylesheet by name later
  return {
    name,
    CSSStyleSheet: style.sheet,
  };
};

const addCSSRule = (sheet, selector, rules, index = 0) => {
  try {
    if ('insertRule' in sheet) {
      return sheet.insertRule(selector + '{' + rules + '}', index);
    } else if ('addRule' in sheet) {
      return sheet.addRule(selector, rules, index);
    }
  } catch (e) {
    return e;
  }
};
export {
   create,
   addCSSRule,
};
