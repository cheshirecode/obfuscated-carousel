"use strict"
const Path = require('path');
const Webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ExtractSASS = new ExtractTextPlugin('styles/bundle.css');
const path = require('path');

module.exports = (options) => {
  let webpackConfig = {
    devtool: options.devtool,
    entry: [
      `webpack-dev-server/client?http://localhost:${options.port}`,
      'webpack/hot/dev-server',
      './src/js/index',
    ],
    output: {
      path: Path.join(__dirname, 'build'),
      filename: 'bundle.js',
    },
    plugins: [
      new Webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(options.isProduction ? 'production' : 'development'),
        },
      }),
      new HtmlWebpackPlugin({
        template: './src/index.html',
      }),
    ],
    resolve: {
      root: path.join(__dirname, './src'),
    },
    resolveLoader: {
      root: path.join(__dirname, './node_modules'),
    },
    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel-loader',
        },
        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'url-loader?limit=10000&minetype=application/font-woff',
        },
        {
          test: /\.(ico)(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'file?name=fonts/[name].[ext]',
        },
        {
          test: /\.(ttf|eot|svg|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'file-loader',
        },
      ],
    },
  };

  if (options.isProduction) {
    webpackConfig.entry = ['./src/js/index'];

    webpackConfig.plugins.push(
      new Webpack.optimize.OccurenceOrderPlugin(),
      new Webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false,
        },
      }),
      ExtractSASS
    );

    webpackConfig.module.loaders.push({
      test: /\.(scss|css|sass)$/i,
      loader: ExtractSASS.extract(['css',
        'sass',
        'sass?outputStyle=expanded&includePaths[]='
          + path.resolve(__dirname, './node_modules/compass-mixins/lib'),
      ]),
    });
  } else {
    webpackConfig.plugins.push(
      new Webpack.HotModuleReplacementPlugin()
    );

    webpackConfig.module.loaders.push({
      test: /\.(scss|css)$/i,
      loaders: [
        'style',
        'css',
        'sass',
        'sass?outputStyle=expanded&includePaths[]='
          + path.resolve(__dirname, './node_modules/compass-mixins/lib'),
      ],
    }, {
      test: /\.(less)$/i,
      loaders: [
        'style',
        'css',
        'less',
      ],
    },
    {
      test: /\.js$/,
      loader: 'eslint',
      exclude: /node_modules/,
    });

    webpackConfig.devServer = {
      contentBase: './build',
      hot: true,
      port: options.port,
      inline: true,
      progress: true,
      proxy: {
        '/ajax': {
          target: 'https://www.propertyfinder.ae/en/find-broker',
          ignorePath: false,
          changeOrigin: true,
          secure: false,
        },
      },
    };
  }
  return webpackConfig;
};
