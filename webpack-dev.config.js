module.exports = require('./webpack.config-helper')({
  isProduction: false,
  devtool: 'cheap-module-eval-source-map',
  port: 2337,
});
