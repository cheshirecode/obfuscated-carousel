# Problem
Use endpoint https://www.propertyfinder.ae/en/find-broker/ajax/search?page=1 to display a carousel similar to http://v4-alpha.getbootstrap.com/components/carousel/#example.

# What I used
- https://bitbucket.org/cheshirecode/quick-es6-webpack-prototyping
- compass
- font-awesome
- Vanilla JS in ES6.

# Instructions
### Installation
`npm i` OR `yarn`

### Start dev environment (Recommended due to problem below)

```
npm run dev
```

### Build Prod Version
Got this error
```
XMLHttpRequest cannot load https://www.propertyfinder.ae/en/find-broker/ajax/search?page=1. No 'Access-Control-Allow-Origin' header is present on the requested resource. Origin 'http://localhost:1337' is therefore not allowed access.
```
hence build version can only work if Access-Control-Allow-Origin is enabled on Backend server.
```
npm run build
```